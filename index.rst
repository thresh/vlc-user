.. VLC User Documentation master file, created by
   sphinx-quickstart on Mon Sep  9 19:49:25 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.
   
######################
VLC User Documentation
######################
 
`VLC media player <https://www.videolan.org/vlc/>`_ (commonly known as just **VLC**) is a free and open source cross-platform multimedia player and framework that plays most multimedia files as well as DVDs, Audio CDs, VCDs,and various streaming protocols. You can download the latest version of VLC on our `website <https://www.videolan.org/vlc/#download>`_ for free. 

************
Key Features
************

* VLC plays Files, Discs, Webcams, Devices and Streams.
* VLC runs on all platforms and is completely free. 
* It has the most complete feature-set over the video, subtitle synchronisation, video and audio filters.
* It has hardware decoding on most platforms. It supports 0-copy on the GPU and can fallback on software when required.
* No spywares, ads and user tracking are allowed on the VLC media player.
* Advanced formats are allowed on VLC.
* VLC lets you apply audio effects, video effects, and tweak the way a video’s audio and video line up.


***********
First Steps
***********

To get the most out of the VLC media player, start by reviewing a few introductory topics below;

* :ref:`Setup <setup>` - Quickly find and install the appropriate VLC media player for your platform.

* :ref:`User Interface <doc_user_interface>` - Introduction to the UI, and commands of the VLC media player.

* :ref:`Settings <settings>` - Customize VLC to suit your needs.

* :ref:`Tips and Tricks <tips_and_tricks>` - Jump right in with Tips and Tricks to become a VLC power user.

* :ref:`Add-ons <addons>` - Find third-party software programs that can be added to VLC for additional features and abilities.

* :ref:`FAQ <faq>`- Find all frequently asked questions by VLC users.

* :ref:`Support Guide<support>` - Solve your VLC issues right now!

* :ref:`Glossary<glossary>` - Definitions for terms used in VLC and this documentation.


.. toctree::
   :maxdepth: 1
   :hidden:
   
   gettingstarted/index.rst
   userguide/index.rst
   settings/index.rst
   tipsandtricks/index.rst
   addons/index.rst
   support/index.rst
   glossary/index.rst
