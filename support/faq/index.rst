.. _faq:

####
FAQs
####

Have other challenges or don't see your issue outlined below? Please `report a bug or request a feature <https://wiki.videolan.org/Report_bugs>`_  and `join the forum <https://forum.videolan.org/>`_ for discussions.

.. toctree::
   :maxdepth: 3
   
   videolan.rst
   vlcmediaplayer.rst
   faqmacos.rst
   faqwindows.rst
   faqlinux.rst
   legalconcerns.rst